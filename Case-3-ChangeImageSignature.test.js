///<reference types="cypress" />

describe('Directing to Web App', () => {

    it('Opening the Web App', () => {

        cy.viewport(1280, 720)
        cy.visit('oauth.privy.id')

        // const token = "eyJhbGciOiJSUzUxMiIsImtpZCI6InhDZlRaX01Id2J3UTdYc2JCeUlxdm1WQzVmN1kxQzJhcHJCSGZoMXVxVXMifQ.eyJpc3MiOiJQcml2eUlEIFByb3ZpZGVyIiwic3ViIjoiUHJpdnlJRCBQcm92aWRlciIsImlhdCI6MTY1MjM0NTg4MCwiZXhwIjoxNjUyMzUzMDgwLCJqdGkiOiI5NTJjMWI3MC1kNDhjLTQ3ZWQtYWU0Mi0zMWI5ZTAxOTUwNzkiLCJ1aWQiOiJNVDYwNTgiLCJ1c2VyIjp7InByaXZ5SWQiOiJNVDYwNTgiLCJ1dWlkIjoiNzdlMGEyYzUtYzU0MS00ZDBiLWExMzEtZDgxNWMzNWEzNWZhIn0sImNsYWltcyI6WyJ3cml0ZSIsInJlYWQiLCJwdWJsaWMiLCJtYW5hZ2UiXX0.A487pkNsi5aLf1E1Dk2ECx01Y2rtvLIV1NeGj8gFhqYnLGTA_nx0YTRh0Z1kLBdEvfyzomDaXIVumi7ZSolgDfzaGgEqGSMnIqZn7JCUT39HpeGwzuksKzAOR_5abkmXTB30Hj3s9tVtKTUNjIBd3HawRYNjW4XsSNShbSODalcOTwDW2S3To_-vIGuq50geCyiAHZCwpV8X5_kw44y0LzB8Dz7tLwmpwxiy4rRrrpVMU88QTeOiWiYfbeFEyE_QwaigvSxsIJMaQM7BZjQLK-z9ZPBRlJvbv7d5WKBFp6nbWicdBQD_mJ-yiATlCE3wHAKJxb_qdAEnBiMuqp5D_A"

        // cy.then(() => {
        //     window.localStorage.setItem('__auth__token', token)
        // })

    })

    it('Directing to Log-In Page', () => {

        cy.contains('Log In').should('exist')

    })

})

describe('Logging-in With a pre-made Account', () => {

    it('Inserting PrivyID', () => {
        cy.get('[name="user[privyId]"]')
            .type('MT6058').should('have.value', 'MT6058')

        // for (let n = 0; n < 6; n++) {

        cy.get('.btn-danger').trigger('mouseover').click();


        cy.get('[name="user[secret]"]')
            .type('Riyan963123').type('{enter}')

    })



    it('Logging In', () => {
        cy.url('app.privy.id').should('include', '/dashboard')
    })

    it('Logging In', () => {
        cy.contains('Change My Signature Image ').click()
        cy.contains('Add Signature').click({ force: true })
        cy.contains('Image').click()
        cy.contains('Browse').click()

        const filePath = 'TandaTangan.png';
        cy.get('input[name="image"]').attachFile(filePath)
        cy.contains('Crop').click()
        cy.contains('Apply').click()

        cy.get('#__BVID__235 > div > div > div').click()
        const filePath2 = 'TandaTangan.png';
        cy.get('input[name="image"]').attachFile(filePath2)
        cy.contains('Crop').click()
        cy.contains('Apply').click()
        cy.contains('Save').click()
        cy.visit('https://app.privy.id')

    })


})
