/// <reference types="cypress" />

describe('Request From Other', () => {
    it.only('sign', () => {
        cy.visit('https://privy.id/')
        cy.contains('Login').click({
            force: true
        })
        cy.contains('Log In').should('exist')
        cy.get('input[name="user[privyId]"]').type('MT6058', {
            force: true
        })
        cy.contains('CONTINUE').click()
        cy.get('input[name="user[secret]"]').type('Riyan963123', {
            force: true
        })
        cy.contains('CONTINUE').click()
        cy.get('#v-step-0').click()
        cy.contains('Request From Others ').click()
        cy.contains('Drag your document here or click browse').click()

        const fileName = 'Doc1.pdf';

        cy.get('[type="file"]').attachFile({
            filePath: fileName,
            encoding: 'base64'
        })
        cy.get('.modal-content .modal-footer button:contains("Upload")').click()
        cy.wait(2000)
        cy.contains('Continue').click()
        cy.contains('Click to add recipient').click()
        cy.get('[placeholder="Enter PrivyID"]').type('FL9075', { force: true })
        cy.wait(2000)
        cy.contains('ZD8298').click()
        cy.wait(1000)
        cy.get('[type="radio"]').check({ force: true })
        cy.wait(2000)
        cy.contains('- Select Role -').click()
        cy.wait(3000)
        cy.contains('Signer').click({ force: true })
        cy.wait(2000)
        cy.contains('button', 'Add Recipient').click({ force: true })
        cy.wait(2000)
        cy.contains('button', 'Continue').click()
        cy.wait(4000)
        cy.contains('button', 'Set Signature').click()
        cy.contains('Felix Fathahillah').click()
        cy.contains('Done').click()
    })

})
